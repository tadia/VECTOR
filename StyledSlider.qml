import QtQuick

Rectangle {
    id: root;

    height: 20;
    color: "#225705";

    property var changed;
    property real value;

    Rectangle {
        id: handle;
        width: 32;
        height: 32;
        x: -6 + (root.width - 20) * root.value;
        y: -6;
        border.width: 2;
        border.color: "black";
        radius: 2;
        color: "white";

        MouseArea {
            anchors.fill: parent;

            property int startX;

            onPressed: {
                handle.color = "black";
                startX = mouseX;
            }

            onPositionChanged: {
                var halfSize = this.width / 2;

                parent.x += mouseX - startX;

                if (parent.x < -6) parent.x = -6;
                if (parent.x > root.width - 26) parent.x = root.width - 26;

                if (changed) changed((parent.x + 6) / (root.width - 20));
            }

            onReleased: {
                handle.color = "white";
            }
        }
    }

    function update() {
        handle.x = -6 + (root.width - 20) * root.value;
    }

    onWidthChanged: update();
}
