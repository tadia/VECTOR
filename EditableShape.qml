import QtQuick
import QtQuick.Shapes

import "logic.js" as Logic
import "boundingbox_logic.js" as BBLogic
import "sharedState.js" as Shared

Shape {
    id: root;
    containsMode: Shape.FillContains;

    // Helper properties for transforming shape
    property real oldX;
    property real oldY;
    property real oldWidth;
    property real oldHeight;

    // Properties containing the real dimensions of this object
    property real realX;
    property real realY;
    property real realWidth;
    property real realHeight;

    property bool isSelected: false;
    property var shape;
    property int type;

    preferredRendererType: Shape.CurveRenderer;

    function setColor(color) {
        shape.fillColor = color;
    }
}
