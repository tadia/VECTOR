import QtQuick

import "boundingbox_logic.js" as BBLogic
import "logic.js" as Logic
import "sharedState.js" as Shared

Rectangle {
    id: root;
    width: 32;
    height: 32;
    border.width: 2;
    border.color: "black";
    radius: 2;

    property int role;

    MouseArea {
        anchors.fill: parent;
        property int startX;
        property int startY;

        onPressed: {
            startX = mouseX
            startY = mouseY
            parent.color = "black";

            Logic.startRepositionSelection();
        }

        onPositionChanged: {
            parent.x += mouseX - startX;
            parent.y += mouseY - startY;
            /*pq1.controlX = parent.x + this.width / 2;
            pq1.controlY = parent.y + this.height / 2;*/

            var bbox = parent.parent;
            var halfSize = this.width / 2;

            if (role >= 0 && role <= 2) {
                var diff = parent.y + halfSize;

                bbox.height -= diff;
                bbox.y = bbox.y + diff;
            } else if (role >= 4 && role <= 6) bbox.height = parent.y + halfSize;

            if (role == 0 || role == 6 || role == 7) {
                var diff = parent.x + halfSize;

                bbox.width -= diff;
                bbox.x = bbox.x + diff;
            } else if (role >= 2 && role <= 4) bbox.width = parent.x + halfSize;

            BBLogic.repositionAll(bbox);
            Logic.repositionSelection();
        }

        onReleased: {
            var bbox = parent.parent;

            // Fix negative sizes
            if (bbox.width < 0) {
                bbox.width = -bbox.width;
                bbox.x -= bbox.width;
            }

            if (bbox.height < 0) {
                bbox.height = -bbox.height;
                bbox.y -= bbox.height;
            }
            BBLogic.repositionAll(bbox);

            parent.color = "white";
        }
    }

    function reposition() {
        var halfSize = this.width / 2;

        if (role >= 0 && role <= 2) this.y = -halfSize;
        else if (role == 3 || role == 7) this.y = parent.height / 2 - halfSize;
        else this.y = parent.height - halfSize;

        if (role == 0 || role == 6 || role == 7) this.x = -halfSize;
        else if (role >= 2 && role <= 4) this.x = parent.width - halfSize;
        else this.x = parent.width / 2 - halfSize;
    }
}
