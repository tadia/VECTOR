import QtQuick

import "curve_logic.js" as CurveLogic

Rectangle {
    id: root;

    width: 32;
    height: 32;
    radius: 2;
    color: "black";

    property int num;

    Image {
        anchors.centerIn: parent;
        source: "/res/plus.svg";
    }

    MouseArea {
        anchors.fill: parent;

        onPressed: {
            parent.color = "#225705";
        }

        onReleased: {
            root.destroy();
            CurveLogic.newSpline(num);
        }
    }
}
