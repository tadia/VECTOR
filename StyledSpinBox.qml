import QtQuick
import QtQuick.Controls

Rectangle {
    id: root;

    property int value;
    property int from;
    property int to;

    property var changed;

    height: 32;
    color: "white";

    TextButtonVisible {
        id: minus;
        anchors.left: parent.left;
        textIcon: "-";
        width: parent.height;

        function decrement(button) {
            value--;
            if (value < from) value = from;
            field.text = value;
            if (changed) changed();
        }

        onClick: decrement;
        onHold: decrement;
    }

    TextButtonVisible {
        id: plus;
        anchors.right: parent.right;
        textIcon: "+";
        width: parent.height;

        function increment(button) {
            value++;
            if (value > to) value = to;
            field.text = value;
            if (changed) changed();
        }

        onClick: increment;
        onHold: increment;
    }


    TextField {
        id: field;
        text: value;

        anchors.top: parent.top;
        anchors.bottom: parent.bottom;
        anchors.left: minus.right;
        anchors.right: plus.left;

        background: Rectangle {
            color: "white"
        }

        verticalAlignment: TextInput.AlignVCenter;
        horizontalAlignment: TextInput.AlignHCenter;

        inputMethodHints: Qt.ImhDigitsOnly;
        onTextEdited: {
            root.value = parseInt(text);
            if (root.value < root.from) root.value = root.from;
            if (root.value > root.to) root.value = root.to;
            text = value;
            if (changed) changed();
        }

        font.pointSize: 12;
        color: "black";
    }
}
