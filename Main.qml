import QtQuick
import QtQuick.Shapes
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs

import FileIO 1.0

import "logic.js" as Logic
import "popup_logic.js" as PopLogic
import "sharedState.js" as Shared
import "curve_logic.js" as CurveLogic
import "file_logic.js" as FileLogic

Window {
    id: window
    width: 640
    height: 480
    visibility: Window.Maximized;
    title: qsTr("Vektorový editor")

    color: "#ccc";

    IconButton {
        iconSource: "/res/open.svg";

        anchors.bottom: parent.bottom;
        anchors.left: parent.left;
        anchors.bottomMargin: 4;
        anchors.leftMargin: 4;
        z: 3

        width: 40;
        radius: 4;

        onClick: function(button) {
            sidebar.state = "open";
        }
    }

    Rectangle {
        id: sidebar;
        z: 3;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;
        width: 48;
        color: "black";
        state: "closed";

        ColumnLayout {
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.right: parent.right;
            spacing: 0

            IconButton {
                iconSource: "/res/move.svg";

                width: parent.width;

                onClick: function(button) {
                    if (Shared.mode == 0) {
                        Shared.mode = 1;
                        iconSource = "/res/eye.svg";
                    } else {
                        Shared.mode = 0;
                        iconSource = "/res/move.svg";
                    }
                    Logic.deselect();
                    Shared.canvas.switchMode();
                }

                Component.onCompleted: Shared.switcher = this;
            }

            Rectangle {
                width: parent.width
                height: 1
                color: "#999"
            }

            IconButton {
                iconSource: "/res/shape-add.svg"

                width: parent.width

                onClick: function(button) {
                    if (Shared.mode == 0) PopLogic.openPopup(56, y, 1, window);
                }
            }

            IconButton {
                iconSource: "/res/intersect.svg"

                width: parent.width

                onClick: function(button) {
                    if (Shared.mode == 0) PopLogic.openPopup(56, y, 2, window);
                }
            }

            IconButton {
                iconSource: "/res/color.svg"

                width: parent.width

                onClick: function(button) {
                    if (Shared.selectedObjects.length > 0) {
                        sidebar.state = "closed";
                        PopLogic.closePopup();
                        PopLogic.openParamEditor();
                    }
                }
            }

            IconButton {
                iconSource: "/res/file.svg"

                width: parent.width

                onClick: function(button) {
                    PopLogic.openPopup(56, y, 3, window);
                }
            }
        }

        IconButton {
            iconSource: "/res/close.svg";

            anchors.bottom: parent.bottom;
            anchors.left: parent.left;
            anchors.right: parent.right;

            width: parent.width;

            onClick: function(button) {
                button.parent.state = "closed";
                PopLogic.closePopup();
            }

            Rectangle {
                anchors.bottom: parent.top;
                anchors.left: parent.left;
                width: parent.width
                height: 1
                color: "#999"
            }
        }

        states: [
            State {
                name: "open"
                PropertyChanges { target: sidebar; x: 0 }
            },
            State {
                name: "closed"
                PropertyChanges { target: sidebar; x: -48 }
            }
        ]

        transitions: [
            Transition {
                from: "*"
                to: "*"

                NumberAnimation {
                    property: "x"
                    duration: 150
                    easing.type: Easing.OutQuad
                }
            }
        ]
    }

    Rectangle {
        id: curveSidebar;
        z: 4;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;
        width: 48;
        color: "black";
        state: "closed";

        ColumnLayout {
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.right: parent.right;
            spacing: 0

            IconButton {
                iconSource: "/res/square.svg"

                width: parent.width

                onClick: function(button) {
                    CurveLogic.resymmetrifySelectedSegments(true);
                }
            }

            IconButton {
                iconSource: "/res/independent.svg"

                width: parent.width

                onClick: function(button) {
                    CurveLogic.resymmetrifySelectedSegments(false);
                }
            }

            IconButton {
                iconSource: "/res/circle.svg"

                width: parent.width

                onClick: function(button) {
                    CurveLogic.decurvifySelectedSegments();
                }
            }

            IconButton {
                iconSource: "/res/delete.svg"

                width: parent.width

                onClick: function(button) {
                    CurveLogic.deleteSelectedSegments();
                }
            }

            IconButtonSplineAdd {
                width: parent.width
            }
        }
        states: [
            State {
                name: "open"
                PropertyChanges { target: curveSidebar; x: 0 }
            },
            State {
                name: "closed"
                PropertyChanges { target: curveSidebar; x: -48 }
            }
        ]

        transitions: [
            Transition {
                from: "*"
                to: "*"

                NumberAnimation {
                    property: "x"
                    duration: 150
                    easing.type: Easing.OutQuad
                }
            }
        ]

        Component.onCompleted: Shared.curveSidebar = this;
    }

    property var sidebar: sidebar;

    VectorCanvas {
        id: canvas;
        anchors.fill: parent;

        VectorBoundary {
            x: 0;
            realX: 0;
            y: 0;
            realY: 0;
            width: 420;
            realWidth: 420;
            height: 420;
            realHeight: 420;
            Component.onCompleted: Shared.boundary = this;
        }

        Component.onCompleted: {
            Shared.canvas = this;
        }
    }

    // File dialogs
    FileDialog {
        fileMode: FileDialog.SaveFile;
        modality: Qt.WindowModal;
        Component.onCompleted: Shared.dialogs.export = this;
        defaultSuffix: "svg";
        nameFilters: ["Scalable Vector Graphics (*.svg)"]

        FileIO {
            id: exportFileIO;
            onError: console.log(msg);
        }
        property var fileIO: exportFileIO;

        onAccepted: FileLogic.finishExportFile();
    }

    FileDialog {
        fileMode: FileDialog.SaveFile;
        modality: Qt.WindowModal;
        Component.onCompleted: Shared.dialogs.save = this;
        defaultSuffix: "tsf";
        nameFilters: ["Tádův Speciální Formát (*.tsf)"]

        FileIO {
            id: saveFileIO;
            onError: console.log(msg);
        }
        property var fileIO: saveFileIO;

        onAccepted: FileLogic.finishSaveFile();
    }

    FileDialog {
        fileMode: FileDialog.OpenFile;
        modality: Qt.WindowModal;
        Component.onCompleted: Shared.dialogs.open = this;
        defaultSuffix: "tsf";
        nameFilters: ["Tádův Speciální Formát (*.tsf)"]

        FileIO {
            id: openFileIO;
            onError: console.log(msg);
        }
        property var fileIO: openFileIO;

        onAccepted: FileLogic.finishOpenFile();
    }

    Timer {
        running: false
        repeat: false
        interval: 0

        onTriggered: {
            Qt.createComponent("NewFileDialog.qml").createObject(Shared.window, {});
        }

        Component.onCompleted: {
            Shared.newFileTimer = this;
        }
    }

    Component.onCompleted: {
        var splineType = Qt.createComponent("CurveSpline.qml");
        Shared.window = this;
    }
}
