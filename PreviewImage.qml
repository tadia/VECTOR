import QtQuick

import "view_logic.js" as ViewLogic
import "sharedState.js" as Shared

Image {
    id: root;

    property var pincher;
    property var dragger;

    PinchHandler {
        id: pinchy;
        rotationAxis.enabled: false;
        onActiveChanged: {
            if (!active && !draggy.active) ViewLogic.recalc();
        }
    }

    DragHandler {
        id: draggy;
        onActiveChanged: {
            if (!active && !pinchy.active) ViewLogic.recalc();
        }
    }

    Component.onCompleted: {
        pincher = pinchy;
        dragger = draggy;
    }
}
