import QtQuick

import "logic.js" as Logic
import "boundingbox_logic.js" as BBLogic
import "sharedState.js" as Shared
import "popup_logic.js" as PopLogic
import "view_logic.js" as ViewLogic
import "curve_logic.js" as CurveLogic

Rectangle {
    id: root;

    color: "#ccc";

    property var previewImage;
    clip: true;

    MouseArea {
        id: ma;
        z: 1;
        anchors.fill: parent;

        property var startX: null;
        property var startY: null;
        property var timer;
        property bool gotMoved;
        property bool accidentProtection;
        property var target: null;

        onPressed: {
            target = null;

            if (Shared.openedPopup || Shared.mode !== 0 || Shared.paramEditor) return;
            if (Shared.drawObject !== null) {
                if (Shared.drawObject == 2) {
                    CurveLogic.createCurveSegment(mouseX, mouseY);
                    ma.timer = Date.now();
                    startX = mouseX;
                    startY = mouseY;
                } else Logic.createDrawBox(mouseX, mouseY);
                return;
            }
            if (Shared.currentCurve) return;

            Shared.objects.forEach(function(shape) {
                if (shape.contains(Qt.point(mouseX - shape.x, mouseY - shape.y))) target = shape;
            });

            if (target) {
                startX = mouseX;
                startY = mouseY;
                gotMoved = false;
                accidentProtection = true;
                ma.timer = Date.now();
            }
        }

        onReleased: {
            if (Shared.paramEditor) return;
            if (Shared.currentCurveSegment) CurveLogic.finalizeCurveSegment(mouseX, mouseY, Date.now() - ma.timer <= 150 && (Math.sqrt(Math.pow(mouseX - startX, 2) + Math.pow(mouseY - startY, 2)) < 4));
            else if (Shared.currentCurve) {
                if (Shared.addNewSpline) {
                    CurveLogic.toggleAddingSplines()
                } else if (Shared.selectedCurveHandles.length > 0) {
                    Shared.selectedCurveHandles.forEach(function(handle) {
                        handle.deselect();
                    });
                    Shared.selectedCurveHandles = [];
                } else CurveLogic.stopEditingCurve();
            }

            if (Shared.drawBox) {
                Logic.addShapeFinal(root, Shared.drawObject, Shared.drawBox.x, Shared.drawBox.y, Shared.drawBox.width, Shared.drawBox.height);
                Shared.drawBox.destroy();
                Shared.drawObject = null;
                Shared.drawBox = null;
            }

            if (target) {
                if (Date.now() - ma.timer <= 225 && !gotMoved) {
                    Logic.select(target, root.parent);
                } else if (!gotMoved) {
                    Logic.multiSelect(target, root.parent);
                }

                startX = null;
                startY = null;
                target = null;
            } else {
                Logic.deselect();
                PopLogic.closePopup();
            }
        }

        onPositionChanged: {
            if (Shared.currentCurveSegment) CurveLogic.repositionCurveSegment(mouseX, mouseY);

            if (Shared.drawBox) {
                Shared.drawBox.width = mouseX - Shared.drawBox.x;
                Shared.drawBox.height = mouseY - Shared.drawBox.y;
            }

            if (target) {
                if (startX === null || accidentProtection && (Math.sqrt(Math.pow(mouseX - startX, 2) + Math.pow(mouseY - startY, 2)) < 4)) {
                    return;
                }

                accidentProtection = false;
                if (!gotMoved) {
                    if (Date.now() - ma.timer <= 225) {
                        Logic.moveSelect(target, root.parent);
                    } else {
                        Logic.multiSelect(target, root.parent);
                    }
                }

                Logic.moveSelectionBy(target, mouseX - startX, mouseY - startY);
                startX = mouseX;
                startY = mouseY;

                BBLogic.drawBoundingBox(root.parent);

                gotMoved = true;
            }
        }
    }

    function switchMode() {
        ViewLogic.switchMode(root, ma);
    }

    Timer {
        id: timer
        running: false
        repeat: false
        interval: 0

        onTriggered: {
            CurveLogic.editCurve(Shared.selectedObjects[0]);
            Logic.deselect();
        }

        Component.onCompleted: {
            Shared.timer = this;
        }
    }
}
