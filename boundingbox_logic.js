.import "sharedState.js" as Shared

var handleType = Qt.createComponent("BoundingBoxHandle.qml");
var boundType = Qt.createComponent("BoundingBox.qml");

function drawBoundingBox(canvas) {
    if (Shared.selectedObjects.length == 0) return;

    var x1 = null;
    var y1 = null;
    var x2 = null;
    var y2 = null;

    Shared.selectedObjects.forEach(function (shape) {
        if (x1 == null) {
            if (shape.type == 2) {
                x1 = shape.x + shape.boundingRect.x;
                y1 = shape.y + shape.boundingRect.y;
                x2 = shape.x + shape.boundingRect.width + shape.boundingRect.x;
                y2 = shape.y + shape.boundingRect.height + shape.boundingRect.y;
            } else {
                x1 = shape.x;
                y1 = shape.y;
                x2 = shape.width + shape.x;
                y2 = shape.height + shape.y;
            }
        } else {
            if (shape.type == 2) {
                x1 = Math.min(x1, shape.x + shape.boundingRect.x);
                y1 = Math.min(y1, shape.y + shape.boundingRect.y);
                x2 = Math.max(x2, shape.x + shape.boundingRect.width + shape.boundingRect.x);
                y2 = Math.max(y2, shape.y + shape.boundingRect.height + shape.boundingRect.y);
            } else {
                x1 = Math.min(x1, shape.x);
                y1 = Math.min(y1, shape.y);
                x2 = Math.max(x2, shape.width + shape.x);
                y2 = Math.max(y2, shape.height + shape.y);
            }
        }
    });

    if (Shared.boundingBox == null) {
        Shared.boundingBox = boundType.createObject(null, { "parent": canvas, "x": x1, "y": y1, "width": x2 - x1, "height": y2 - y1 });
    } else {
        Shared.boundingBox.x = x1;
        Shared.boundingBox.y = y1;
        Shared.boundingBox.width = x2 - x1;
        Shared.boundingBox.height = y2 - y1;
        Shared.boundingBox.repositionAll();
    }
}

function createHandles(box) {
    for (var i = 0; i < 8; i++) {
        var handle = handleType.createObject(box, { "role": i, "x": 0, "y": 0 });
        handle.reposition();
        box.handles.push(handle);
    }
}

function repositionAll(box) {
    var handles = box.handles;
    for (var i = 0; i < handles.length; i++) {
        handles[i].reposition();
    }
}
