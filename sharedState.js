.pragma library

var objects = [];
var selectedObjects = [];

var boundingBox = null;
var drawBox = null;

var openedPopup = null;
var canvas = null;
var window = null;
var boundary;
var drawObject = null;

var transformX = 0;
var transformY = 0;
var transformScale = 1;

var curveSegments = [];
var currentCurveSegment = null;
var previousCurveSegment = null;
var currentCurve = null;
var finishCurveOpen = false;
var finishCurve = false;
var curveSidebar;
var switcher;
var selectedCurveHandles = [];
var addNewSpline = false;
var addNewSplineButton;
var newSplineButtons = [];

var timer;
var newFileTimer;

var paramEditor = null;
var mode = 0;

var dialogs = {
    "save": null,
    "open": null,
    "export": null
}

var popups = [
    {
        "type": 1,
        "selected": 0,
        "options": [
            {
                "text": "Režim úpravy objektů",
                "iconSource": "/res/move.svg"
            },
            {
                "text": "Režim zobrazení",
                "iconSource": "/res/eye.svg"
            }
        ]
    },
    {
        "type": 0,
        "options": [
            {
                "text": "Přidat čtverec",
                "iconSource": "/res/square.svg"
            },
            {
                "text": "Přidat elipsu",
                "iconSource": "/res/circle.svg"
            },
            {
                "text": "Přidat křivku",
                "iconSource": "/res/curve.svg"
            }
        ]
    },
    {
        "type": 0,
        "options": [
            {
                "text": "Přesunout navrch",
                "iconSource": "/res/top.svg"
            },
            {
                "text": "Posunout nahoru",
                "iconSource": "/res/up.svg"
            },
            {
                "text": "Posunout dolů",
                "iconSource": "/res/down.svg"
            },
            {
                "text": "Přesunout dospodu",
                "iconSource": "/res/bottom.svg"
            }
        ]
    },
    {
        "type": 0,
        "options": [
            {
                "text": "Vytvořit nový",
                "iconSource": "/res/file.svg"
            },
            {
                "text": "Otevřít",
                "iconSource": "/res/load.svg"
            },
            {
                "text": "Uložit jako",
                "iconSource": "/res/save.svg"
            },
            {
                "text": "Exportovat",
                "iconSource": "/res/export.svg"
            }
        ]
    }
];
