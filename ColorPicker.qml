import QtQuick
import QtQuick.Layouts;

Rectangle {
    id: root;
    width: height + 32;
    Layout.maximumWidth: width;
    property real hue;
    property real saturation;
    property real value;
    property var changed;

    onHueChanged: {
        if (hue < 0) hue = 0;
        hueHandle.y = hue * height - 16;
    }

    onSaturationChanged: {
        svHandle.x = (1 - saturation) * height - 16;
    }

    onValueChanged: {
        svHandle.y = (1 - value) * height - 16;
    }

    Component.onCompleted: {
        if (hue < 0) hue = 0;
        hueHandle.y = hue * height - 16;
        svHandle.x = (1 - saturation) * height - 16;
        svHandle.y = (1 - value) * height - 16;
    }

    Rectangle {
        height: parent.height;
        width: parent.width;

        gradient: Gradient {
            orientation: Gradient.Horizontal;

            GradientStop { position: 0.0; color: Qt.hsva(hue, 1, 1, 1) }
            GradientStop { position: 1.0; color: "white" }
        }

        Rectangle {
            anchors.fill: parent;

            gradient: Gradient {
                orientation: Gradient.Vertical;

                GradientStop { position: 0.0; color: "transparent" }
                GradientStop { position: 1.0; color: "black" }
            }
        }
    }

    Rectangle {
        width: 32;
        height: parent.height;
        anchors.top: parent.top;
        anchors.right: parent.right;

        gradient: Gradient {
            orientation: Gradient.Vertical;

            GradientStop { position: 0.0; color: "#f00" }
            GradientStop { position: 0.1666; color: "#ff0" }
            GradientStop { position: 0.3333; color: "#0f0" }
            GradientStop { position: 0.5; color: "#0ff" }
            GradientStop { position: 0.6666; color: "#00f" }
            GradientStop { position: 0.8333; color: "#f0f" }
            GradientStop { position: 1.0; color: "#f00" }
        }
    }

    Rectangle {
        id: hueHandle;
        width: 64;
        height: 32;
        x: root.height-16;
        y: -16;
        border.width: 2;
        border.color: "black";
        radius: 2;
        color: Qt.hsva(hue, 1, 1, 1);

        MouseArea {
            anchors.fill: parent;

            property int startX;
            property int startY;

            onPressed: {
                startY = mouseY
            }

            onPositionChanged: {
                var halfSize = this.height / 2;

                parent.y += mouseY - startY;

                if (parent.y < -halfSize) parent.y = -halfSize;
                if (parent.y > root.height - halfSize) parent.y = root.height - halfSize;

                hue = (parent.y + halfSize) / root.height;
                if (changed) changed();
            }
        }
    }

    Rectangle {
        id: svHandle;
        width: 32;
        height: 32;
        x: -16;
        y: -16;
        border.width: 2;
        border.color: "black";
        radius: 2;
        color: Qt.hsva(hue, saturation, value, 1);

        MouseArea {
            anchors.fill: parent;

            property int startX;
            property int startY;

            onPressed: {
                startX = mouseX
                startY = mouseY
            }

            onPositionChanged: {
                var halfSize = this.width / 2;

                parent.x += mouseX - startX;
                parent.y += mouseY - startY;

                if (parent.x < -halfSize) parent.x = -halfSize;
                if (parent.y < -halfSize) parent.y = -halfSize;
                if (parent.x > root.height - halfSize) parent.x = root.height - halfSize;
                if (parent.y > root.height - halfSize) parent.y = root.height - halfSize;

                saturation = 1 - (parent.x + halfSize) / root.height;
                value = 1 - (parent.y + halfSize) / root.height;
                if (changed) changed();
            }
        }
    }
}
