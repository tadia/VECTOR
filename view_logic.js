.import "sharedState.js" as Shared

var previewType = Qt.createComponent("PreviewImage.qml");

function switchMode(canvas, ma) {
    var mode = Shared.mode;

    ma.visible = mode === 0;
    if (mode === 1 && !canvas.previewImage) {
        canvas.grabToImage(function(result) {
            canvas.previewImage = previewType.createObject(canvas.parent, {
                "source": result.url
            });
            canvas.visible = false;
        });
    }
    if (mode === 0 && canvas.previewImage) {
        canvas.previewImage.destroy();
        canvas.previewImage = null;
        canvas.visible = true;
    }
}

function recalc() {
    var img = Shared.canvas.previewImage;

    // Actually reposition all images
    Shared.transformX = img.x + (Shared.canvas.width - Shared.canvas.width * img.scale) / 2 + Shared.transformX * img.scale;
    Shared.transformY = img.y + (Shared.canvas.height - Shared.canvas.height * img.scale) / 2 + Shared.transformY * img.scale;
    Shared.transformScale *= img.scale;

    Shared.objects.forEach(function(shape) {
        shape.shape.strokeWidth = shape.shape.realStrokeWidth * Shared.transformScale;
        shape.recalcDimensions();
    });

    Shared.boundary.recalcDimensions();

    Shared.canvas.grabToImage(function(result) {
        // Reset preview transformation
        img.x = 0;
        img.y = 0;
        img.pincher.persistentScale = 1;
        img.scale = 1;
        Shared.canvas.previewImage.source = result.url;
    });
}
