import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import "logic.js" as Logic

Rectangle {
    id: root;
    color: "white";
    anchors.fill: parent;
    z: 7;

    Rectangle {
        color: "black";
        anchors.centerIn: parent;
        radius: 4;

        width: layout.childrenRect.width + 16;
        height: layout.childrenRect.height + 16;

        Rectangle {
            color: "transparent";
            anchors.fill: parent;
            anchors.margins: 8;

            ColumnLayout {
                id: layout;

                Text {
                    text: "Nový obrázek";
                    color: "white";
                    font.pointSize: 20;
                }

                Rectangle {
                    width: 0;
                    height: 4;
                }

                Text {
                    text: "Šířka";
                    color: "white";
                    font.pointSize: 10;
                }

                TextField {
                    id: widthField;
                    text: "420";
                    Layout.fillWidth: true;
                    height: 32;

                    background: Rectangle {
                        color: "white"
                    }

                    verticalAlignment: TextInput.AlignVCenter;
                    horizontalAlignment: TextInput.AlignHCenter;

                    inputMethodHints: Qt.ImhDigitsOnly;

                    onTextEdited: if (text === "") text = 0;

                    font.pointSize: 12;
                    color: "black";
                }

                Rectangle {
                    width: 0;
                    height: 4;
                }

                Text {
                    text: "Výška";
                    color: "white";
                    font.pointSize: 10;
                }

                TextField {
                    id: heightField;
                    text: "420";
                    Layout.fillWidth: true;
                    height: 32;

                    background: Rectangle {
                        color: "white"
                    }

                    verticalAlignment: TextInput.AlignVCenter;
                    horizontalAlignment: TextInput.AlignHCenter;

                    inputMethodHints: Qt.ImhDigitsOnly;

                    onTextEdited: if (text === "") text = 0;

                    font.pointSize: 12;
                    color: "black";
                }

                Rectangle {
                    width: 0;
                    height: 4;
                }

                Rectangle {
                    id: btn;

                    Layout.fillWidth: true;
                    height: 48;
                    state: "default";

                    Text {
                        text: "Vytvořit";
                        font.pointSize: 12;
                        color: "white";
                        anchors.centerIn: parent;
                    }

                    MouseArea {
                        id: ma
                        anchors.fill: parent;

                        onPressed: btn.state = "pressed";
                        onReleased: {
                            btn.state = "default";
                            Logic.resetContext(Number.parseInt(widthField.text), Number.parseInt(heightField.text));
                            root.destroy();
                        }
                    }

                    states: [
                        State {
                            name: "default"
                            PropertyChanges { target: btn; color: "#225705" }
                        },
                        State {
                            name: "pressed"
                            PropertyChanges { target: btn; color: "#3f9a00" }
                        }
                    ]

                    transitions: [
                        Transition {
                            from: "pressed"
                            to: "default"

                            ColorAnimation {
                                duration: 150
                            }
                        }
                    ]
                }
            }
        }
        Rectangle {
            width: 32;
            height: 32;
            radius: 2;
            color: "black";

            anchors.left: parent.right;
            anchors.top: parent.top;
            anchors.leftMargin: 8;

            Image {
                anchors.centerIn: parent;
                source: "/res/x.svg";
            }

            MouseArea {
                anchors.fill: parent;

                onPressed: {
                    parent.color = "#225705";
                }

                onReleased: {
                    root.destroy();
                }
            }
        }
    }
}
