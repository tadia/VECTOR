import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Rectangle {
    id: controls;

    property int fillRed;
    property int fillGreen;
    property int fillBlue;
    property real fillAlpha;
    property int strokeRed;
    property int strokeGreen;
    property int strokeBlue;
    property real strokeAlpha;
    property real strokeWidth;
    property real strokeStyle;
    property int tab: 0;
    property var updateValues;
    property var closeEditor;
    property var closeEditorStart;
    z: 6;

    Component.onCompleted: {
        updateValues();
        state = "open";
    }

    color: "black";
    x: 0;
    width: parent.width;
    state: "closed";
    height: layout.childrenRect.height + 16;

    Rectangle {
        id: innerRect;
        color: "black";
        anchors.top: parent.top;
        anchors.topMargin: 8;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 8;
        anchors.horizontalCenter: parent.horizontalCenter;
        width: controls.width<416?controls.width-16:400;

        ColumnLayout {
            id: layout;
            anchors.fill: parent;
            spacing: 16;

            Rectangle {
                Layout.fillWidth: true;
                color: "black";
                height: 48;

                IconButtonVisible {
                    id: fillButton;
                    iconSource: "/res/bucket.svg"
                    width: 48;
                    selected: controls.tab == 0;
                    onClick: function() {
                        controls.tab = 0;
                        red.value = controls.fillRed;
                        green.value = controls.fillGreen;
                        blue.value = controls.fillBlue;
                        alpha.value = controls.fillAlpha;
                        opacityText.text = Math.round(controls.fillAlpha * 100);
                        alpha.update();
                        paramLayout.colorChangeByParam();
                    }
                }

                IconButtonVisible {
                    anchors.left: fillButton.right;
                    anchors.leftMargin: 8;
                    iconSource: "/res/ballpen.svg"
                    width: 48;
                    selected: controls.tab == 1;
                    onClick: function() {
                        controls.tab = 1;
                        red.value = controls.strokeRed;
                        green.value = controls.strokeGreen;
                        blue.value = controls.strokeBlue;
                        alpha.value = controls.strokeAlpha;
                        opacityText.text = Math.round(controls.strokeAlpha * 100);
                        alpha.update();
                        paramLayout.colorChangeByParam();
                    }
                }

                Timer {
                    id: closeTimer;
                    interval: 150;
                    running: false;
                    repeat: false;
                    onTriggered: {
                        controls.closeEditor();
                    }
                }

                IconButtonVisible {
                    anchors.right: parent.right;
                    iconSource: "/res/x.svg"
                    width: 48;
                    onClick: function() {
                        controls.state = "closed";
                        closeTimer.start();
                        if (controls.closeEditorStart) controls.closeEditorStart();
                    }
                }
            }

            ColumnLayout {
                Layout.fillWidth: true;
                enabled: controls.tab == 1;
                opacity: controls.tab == 1?1:0;

                Text {
                    text: "Tloušťka";
                    color: "white";
                    font.pointSize: 10;
                }

                TextField {
                    id: width;
                    Layout.fillWidth: true;
                    height: 32;

                    text: controls.strokeWidth;

                    background: Rectangle {
                        color: "white"
                    }

                    verticalAlignment: TextInput.AlignVCenter;
                    horizontalAlignment: TextInput.AlignHCenter;

                    onTextEdited: {
                        var textValue = parseFloat(text);
                        if (Number.isNaN(textValue)) textValue = 0;
                        text = textValue;

                        controls.strokeWidth = textValue;
                        controls.updateValues();
                    }

                    font.pointSize: 12;
                    color: "black";
                }
            }

            ColumnLayout {
                Layout.fillWidth: true;
                id: alphaLayout;

                Text {
                    text: "Průhlednost";
                    color: "white";
                    font.pointSize: 10;
                }

                StyledSlider {
                    id: alpha;

                    width: innerRect.width - 80;
                    value: controls.fillAlpha;

                    changed: function(value) {
                        if (controls.tab == 0) controls.fillAlpha = value;
                        else controls.strokeAlpha = value;
                        controls.updateValues();
                        opacityText.text = Math.round(value*100).toString();
                    }

                    TextField {
                        id: opacityText;

                        width: 60;
                        height: 32;
                        anchors.left: parent.right;
                        anchors.top: parent.top;
                        anchors.topMargin: -6;
                        anchors.leftMargin: 20;

                        background: Rectangle {
                            color: "white"
                        }

                        verticalAlignment: TextInput.AlignVCenter;
                        horizontalAlignment: TextInput.AlignHCenter;
                        inputMethodHints: Qt.ImhDigitsOnly;

                        onTextEdited: {
                            /*if (regExp.test(text)) {
                                red.value = parseInt(text.slice(1, 3), 16);
                                green.value = parseInt(text.slice(3, 5), 16);
                                blue.value = parseInt(text.slice(5, 7), 16);
                                paramLayout.colorChangeByParam();
                            }*/
                            var textValue = parseInt(text, 10);
                            if (Number.isNaN(textValue)) textValue = 0;
                            if (textValue < 0) textValue = 0;
                            if (textValue > 100) textValue = 100;
                            alpha.value = textValue / 100;
                            text = textValue;
                            alpha.update();

                            if (controls.tab == 0) controls.fillAlpha = alpha.value;
                            else controls.strokeAlpha = alpha.value;

                            if (controls.updateValues) controls.updateValues();
                        }

                        Component.onCompleted: {
                            text = Math.round(controls.strokeAlpha*100).toString();
                        }

                        font.pointSize: 12;
                        color: "black";
                    }
                }
            }

            Rectangle {
                Layout.fillWidth: true;
                height: paramLayout.childrenRect.height;
                color: "transparent";

                ColorPicker {
                    id: picker;
                    z: 1;

                    height: 192;

                    changed: function() {
                        var color = Qt.hsva(hue, saturation, value, 1);
                        red.value = color.r * 255;
                        green.value = color.g * 255;
                        blue.value = color.b * 255;
                        hex.text = color.toString();

                        if (controls.tab == 0) {
                            controls.fillRed = red.value;
                            controls.fillGreen = green.value;
                            controls.fillBlue = blue.value;
                        } else {
                            controls.strokeRed = red.value;
                            controls.strokeGreen = green.value;
                            controls.strokeBlue = blue.value;
                        }

                        if (controls.updateValues) controls.updateValues();
                    }
                }

                Rectangle {
                    anchors.left: picker.right;
                    anchors.right: parent.right;
                    anchors.leftMargin: 16;
                    height: paramLayout.childrenRect.height;

                    color: "transparent";

                    ColumnLayout {
                        anchors.fill: parent;
                        id: paramLayout;
                        Text {
                            text: "Červená";
                            color: "white";
                            font.pointSize: 10;
                        }

                        StyledSpinBox {
                            id: red;
                            Layout.fillWidth: true;
                            value: -1;
                            from: 0;
                            to: 255;
                            changed: paramLayout.colorChangeByParam;
                        }

                        Rectangle {
                            width: 0;
                            height: 4;
                        }

                        Text {
                            text: "Zelená";
                            color: "white";
                            font.pointSize: 10;
                        }

                        StyledSpinBox {
                            id: green;
                            Layout.fillWidth: true;
                            value: -1;
                            from: 0;
                            to: 255;
                            changed: paramLayout.colorChangeByParam;
                        }

                        Rectangle {
                            width: 0;
                            height: 4;
                        }

                        Text {
                            text: "Modrá";
                            color: "white";
                            font.pointSize: 10;
                        }

                        StyledSpinBox {
                            id: blue;
                            Layout.fillWidth: true;
                            value: -1;
                            from: 0;
                            to: 255;
                            changed: paramLayout.colorChangeByParam;
                        }

                        Rectangle {
                            width: 0;
                            height: 4;
                        }

                        Text {
                            text: "HEX kód";
                            color: "white";
                            font.pointSize: 10;
                        }

                        TextField {
                            id: hex;
                            Layout.fillWidth: true;
                            height: 32;

                            background: Rectangle {
                                color: "white"
                            }

                            verticalAlignment: TextInput.AlignVCenter;
                            horizontalAlignment: TextInput.AlignHCenter;

                            property var regExp: new RegExp("#([0-9A-Fa-f]){6}");

                            onTextEdited: {
                                if (regExp.test(text)) {
                                    red.value = parseInt(text.slice(1, 3), 16);
                                    green.value = parseInt(text.slice(3, 5), 16);
                                    blue.value = parseInt(text.slice(5, 7), 16);
                                    paramLayout.colorChangeByParam();
                                }
                            }

                            font.pointSize: 12;
                            color: "black";
                        }

                        function colorChangeByParam() {
                            var color = Qt.rgba(red.value / 255, green.value / 255, blue.value / 255, 1);
                            picker.hue = color.hsvHue;
                            picker.saturation = color.hsvSaturation;
                            picker.value = color.hsvValue;
                            hex.text = color.toString();

                            if (controls.tab == 0) {
                                controls.fillRed = red.value;
                                controls.fillGreen = green.value;
                                controls.fillBlue = blue.value;
                            } else {
                                controls.strokeRed = red.value;
                                controls.strokeGreen = green.value;
                                controls.strokeBlue = blue.value;
                            }

                            if (controls.updateValues) controls.updateValues();
                        }

                        Component.onCompleted: {
                            red.value = controls.fillRed;
                            green.value = controls.fillGreen;
                            blue.value = controls.fillBlue;
                            colorChangeByParam();
                        }
                    }
                }
            }
        }
    }

    states: [
        State {
            name: "open"
            PropertyChanges { target: controls; y: controls.parent.height - controls.height }
        },
        State {
            name: "closed"
            PropertyChanges { target: controls; y: controls.parent.height }
        }
    ]

    transitions: [
        Transition {
            from: "*"
            to: "*"

            NumberAnimation {
                property: "y"
                duration: 150
                easing.type: Easing.OutQuad
            }
        }
    ]
}
