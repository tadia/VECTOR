import QtQuick

Rectangle {
    color: parent.color;
    width: parent.width;
    height: parent.radius;
    anchors.left: parent.left;
    anchors.right: parent.right;
}
