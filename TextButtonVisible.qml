import QtQuick
import QtQuick.Controls

Rectangle {
    id: root;
    property string textIcon;
    property var onClick;
    property var onHold;
    height: width;
    state: "default";

    Text {
        text: textIcon;
        color: "white";
        font.pointSize: 12;
        anchors.centerIn: parent;
    }

    MouseArea {
        id: ma
        anchors.fill: parent;

        onPressed: {
            root.state = "pressed";
            timer.activates = 0;
            timer.start();
        }

        onReleased: {
            root.state = "default";
            if (onClick && timer.activates < 12) onClick(parent);
            timer.stop();
        }
    }

    Timer {
        id: timer;
        property int activates: 0;
        interval: 20;
        repeat: true;
        onTriggered: {
            activates++;
            if (onHold && timer.activates >= 12) onHold(parent);
        }
    }

    states: [
        State {
            name: "default"
            PropertyChanges { target: root; color: "#225705" }
        },
        State {
            name: "pressed"
            PropertyChanges { target: root; color: "#3f9a00" }
        }
    ]

    transitions: [
        Transition {
            from: "pressed"
            to: "default"

            ColorAnimation {
                duration: 150
            }
        }
    ]
}
