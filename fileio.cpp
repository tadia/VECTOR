#include "fileio.h"
#include <QTextStream>
#include <QFile>
#include <QDataStream>
#include <QtQml/qqmlregistration.h>
#include <QDebug>

FileIO::FileIO(QObject *parent) : QObject(parent) {
    QML_ELEMENT
}

QString FileIO::read() {
    QFile file(mSource);
    QString fileContent;
    if ( file.open(QIODevice::ReadOnly) ) {
        QString line;
        QTextStream t( &file );
        do {
            line = t.readLine();
            fileContent += line;
        } while (!line.isNull());

        file.close();
    } else {
        emit error("Unable to open the file");
        return QString();
    }
    return fileContent;
}

bool FileIO::write(const QString& data)
{
    QFile file(mSource);
    if (!file.open(QFile::WriteOnly | QFile::Truncate))
        return false;

    QTextStream out(&file);
    out << data;

    file.close();

    return true;
}
