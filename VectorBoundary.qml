import QtQuick
import "sharedState.js" as Shared

Rectangle {
    id: root;

    color: "#fff";

    property real realX;
    property real realY;
    property real realWidth;
    property real realHeight;

    z: 0;

    function recalcDimensions() {
        x = realX * Shared.transformScale + Shared.transformX;
        y = realY * Shared.transformScale + Shared.transformY;
        width = realWidth * Shared.transformScale;
        height = realHeight * Shared.transformScale;
    }
}
