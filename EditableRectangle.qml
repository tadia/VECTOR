import "sharedState.js" as Shared

import QtQuick
import QtQuick.Shapes

EditableShape {
    id: root;
    type: 0;

    ShapePath {
        property real realStrokeWidth;
        strokeColor: "transparent"
        startX: 0; startY: 0

        PathLine { id: p1; x: root.width; y: 0 }
        PathLine { id: p2; x: root.width; y: root.height }
        PathLine { id: p3; x: 0; y: root.height }
        PathLine { id: p4; x: 0; y: 0 }

        Component.onCompleted: {
            root.shape = this;
        }
    }

    function recalcDimensions() {
        x = realX * Shared.transformScale + Shared.transformX;
        y = realY * Shared.transformScale + Shared.transformY;
        width = realWidth * Shared.transformScale;
        height = realHeight * Shared.transformScale;
    }
}
