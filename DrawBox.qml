import QtQuick
import QtQuick.Shapes

Shape {
    id: root;

    z: 1;

    ShapePath {
        fillColor: "transparent"
        strokeColor: "black"
        strokeStyle: ShapePath.DashLine
        strokeWidth: 2
        startX: 0; startY: 0

        PathLine { id: p1; x: root.width; y: 0 }
        PathLine { id: p2; x: root.width; y: root.height }
        PathLine { id: p3; x: 0; y: root.height }
        PathLine { id: p4; x: 0; y: 0 }
    }
}
