import QtQuick
import QtQuick.Controls

Rectangle {
    id: root;
    property string iconSource;
    property var onClick;
    property bool selected;
    height: width;
    state: selected?"pressed":"default";

    onSelectedChanged: {
        if (selected) state = "pressed";
        else state = "default";
    }

    Image {
        anchors.centerIn: parent;
        source: iconSource;
        sourceSize.width: 32
        sourceSize.height: 32
    }

    MouseArea {
        id: ma
        anchors.fill: parent;

        onPressed: root.state = "pressed";
        onReleased: {
            if (!selected) root.state = "default";
            onClick(parent);
        }
    }

    states: [
        State {
            name: "default"
            PropertyChanges { target: root; color: "#225705" }
        },
        State {
            name: "pressed"
            PropertyChanges { target: root; color: "#3f9a00" }
        }
    ]

    transitions: [
        Transition {
            from: "pressed"
            to: "default"

            ColorAnimation {
                duration: 150
            }
        }
    ]
}
