.import "sharedState.js" as Shared
.import "logic.js" as Logic
.import "curve_logic.js" as CurveLogic

var curveType = Qt.createComponent("Curve.qml");

function newFile() {
    Shared.newFileTimer.start();
}

function openFile() {
    Shared.dialogs.open.open();
}

function saveFile() {
    Shared.dialogs.save.open();
}

function exportFile() {
    Shared.dialogs.export.open();
}

function finishExportFile() {
    Shared.dialogs.export.fileIO.source = Shared.dialogs.export.selectedFile;

    var io = Shared.dialogs.export.fileIO;
    var width = Shared.boundary.realWidth;
    var height = Shared.boundary.realHeight;

    // Write necessary headers
    var objectString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
    "<svg width=\""+width+"px\" height=\""+height+"px\" version=\"1.1\" viewBox=\"0 0 "+width+" "+height+"\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "<g stroke-linejoin=\"bevel\">\n";

    for (var i = 0; i < Shared.objects.length; i++) {
        var shape = Shared.objects[i];

        objectString += "\t";

        switch(shape.type) {
            case 0:
                objectString += "<rect x=\""+shape.realX+"\" y=\""+shape.realY+"\" width=\""+shape.realWidth+"\" height=\""+shape.realHeight+"\"";
                break;
            case 1:
                objectString += "<ellipse cx=\""+(shape.realX+shape.realWidth/2)+"\" cy=\""+(shape.realY+shape.realHeight/2)+"\" rx=\""+shape.realWidth/2+"\" ry=\""+shape.realHeight/2+"\"";
                break;
            case 2:
                objectString += "<path d=\""+CurveLogic.splinesToRealSVG(shape)+"\"";
                break;
        }

        if (shape.shape.fillColor) objectString += " fill=\""+shape.shape.fillColor+"\"";
        if (shape.shape.strokeColor) objectString += " stroke=\""+shape.shape.strokeColor+"\"";
        if (shape.shape.strokeWidth) objectString += " stroke-width=\""+shape.shape.realStrokeWidth+"\"";
        objectString += "/>\n";
    }

    // Write necessary footers
    io.write(objectString + "</g>\n</svg>");
}

function finishSaveFile() {
    Shared.dialogs.save.fileIO.source = Shared.dialogs.save.selectedFile;

    var io = Shared.dialogs.save.fileIO;
    var width = Shared.boundary.realWidth;
    var height = Shared.boundary.realHeight;

    // Prepare JSON structure
    var exportData = {
        "width": width,
        "height": height,
        "objects": []
    }

    for (var i = 0; i < Shared.objects.length; i++) {
        var shape = Shared.objects[i];

        switch(shape.type) {
            case 0:
            case 1:
                exportData.objects.push({
                    "type": shape.type,
                    "x": shape.realX,
                    "y": shape.realY,
                    "width": shape.realWidth,
                    "height": shape.realHeight,
                    "fillColor": shape.shape.fillColor,
                    "strokeColor": shape.shape.strokeColor,
                    "strokeWidth": shape.shape.realStrokeWidth
                });
                break;
            case 2:
                exportData.objects.push({
                    "type": shape.type,
                    "x": shape.realX,
                    "y": shape.realY,
                    "closed": shape.closed,
                    "svg": shape.svgSplines,
                    "fillColor": shape.shape.fillColor,
                    "strokeColor": shape.shape.strokeColor,
                    "strokeWidth": shape.shape.realStrokeWidth
                });
                break;
        }
    }

    // Write necessary footers
    var rawData = JSON.stringify(exportData);
    io.write(rawData);
}

function finishOpenFile() {
    Shared.dialogs.open.fileIO.source = Shared.dialogs.open.selectedFile;

    var io = Shared.dialogs.open.fileIO;

    // Prepare JSON structure
    var rawData = io.read();
    var importData = JSON.parse(rawData);
    Logic.resetContext(importData.width, importData.height);

    for (var i = 0; i < importData.objects.length; i++) {
        var shape = importData.objects[i];

        switch(shape.type) {
            case 0:
            case 1:
                Logic.addShapeFinal(Shared.canvas, shape.type, shape.x, shape.y, shape.width, shape.height);
                break;
            case 2:
                Shared.objects.push(curveType.createObject(Shared.canvas, {
                    "x": shape.x,
                    "y": shape.y,
                    "svgSplines": shape.svg,
                    "closed": shape.closed,
                    "splined": shape.splined,
                    "z": Shared.objects.length
                }));

                Shared.objects[i].svg.path = CurveLogic.splinesToSVG(Shared.objects[i].svgSplines);
                Shared.objects[i].shape.strokeStyle = Shared.objects[i].path.finishedStrokeStyle;
                Shared.objects[i].generateDimensions();
        }

        Shared.objects[i].shape.fillColor = shape.fillColor;
        Shared.objects[i].shape.strokeColor = shape.strokeColor;
        Shared.objects[i].shape.strokeWidth = shape.strokeWidth;
        Shared.objects[i].shape.realStrokeWidth = shape.strokeWidth;
    }
}
