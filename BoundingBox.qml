import QtQuick

import "boundingbox_logic.js" as BBLogic
import "logic.js" as Logic
import "sharedState.js" as Shared
import "curve_logic.js" as CurveLogic

Rectangle {
    id: root;
    color: "transparent";

    z: 1;

    // Helper properties for transforming selected objects
    property real oldX;
    property real oldY;
    property real oldWidth;
    property real oldHeight;

    DrawBox {
        anchors.fill: parent;
        z: 0;
    }

    property var handles: [];

    function repositionAll() {
        BBLogic.repositionAll(this);
    }

    Component.onCompleted: BBLogic.createHandles(this);

    Rectangle {
        id: deletor;

        width: 32;
        height: 32;
        radius: 2;
        color: "black";

        visible: !Shared.paramEditor;

        anchors.left: parent.right;
        anchors.top: parent.top;
        anchors.leftMargin: 24;
        anchors.topMargin: -16;

        Image {
            anchors.centerIn: parent;
            source: "/res/delete.svg";
        }

        MouseArea {
            anchors.fill: parent;

            onPressed: {
                parent.color = "#225705";
            }

            onReleased: {
                Logic.deleteSelection();
            }
        }
    }

    Rectangle {
        width: 32;
        height: 32;
        radius: 2;
        color: "black";

        anchors.left: deletor.left;
        anchors.top: deletor.bottom;
        anchors.topMargin: 8;

        visible: Shared.selectedObjects.length==1&&Shared.selectedObjects[0].type==2;

        Image {
            anchors.centerIn: parent;
            source: "/res/edit.svg";
        }

        MouseArea {
            anchors.fill: parent;

            onPressed: {
                parent.color = "#225705";
            }

            onReleased: {
                Shared.timer.start();
            }
        }
    }
}
