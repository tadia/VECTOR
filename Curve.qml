import QtQuick
import QtQuick.Shapes

import "logic.js" as Logic
import "curve_logic.js" as CurveLogic
import "sharedState.js" as Shared

EditableShape {
    id: root;

    type: 2;

    preferredRendererType: Shape.GeometryRenderer;

    property bool isNew: true;
    property bool closed;

    property var svgSplines: [];

    ShapePath {
        property real realStrokeWidth;
        id: path;

        strokeWidth: 2;
        strokeColor: "black";
        strokeStyle: ShapePath.DashLine;
        fillColor: "transparent";

        property int finishedStrokeStyle: ShapePath.SolidLine;

        PathSvg { id: svg; path: "" }

        startX: 0; startY: 0;
    }

    property var path: path;
    property var shape: path;
    property var svg: svg;

    function recalcDimensions() {
        x = Logic.realToDisplayX(realX);
        y = Logic.realToDisplayY(realY);

        svgSplines.forEach(function (element) {
            element.x = element.realX * Shared.transformScale;
            element.y = element.realY * Shared.transformScale;
            element.x1 = element.realX1 * Shared.transformScale;
            element.y1 = element.realY1 * Shared.transformScale;
            element.x2 = element.realX2 * Shared.transformScale;
            element.y2 = element.realY2 * Shared.transformScale;
        });

        svg.path = CurveLogic.splinesToSVG(svgSplines);
    }

    function generateDimensions() {
        realX = Logic.displayToRealX(x);
        realY = Logic.displayToRealY(y);

        svgSplines.forEach(function (element) {
            element.realX = element.x / Shared.transformScale;
            element.realY = element.y / Shared.transformScale;
            element.realX1 = element.x1 / Shared.transformScale;
            element.realY1 = element.y1 / Shared.transformScale;
            element.realX2 = element.x2 / Shared.transformScale;
            element.realY2 = element.y2 / Shared.transformScale;
        });
    }

    Timer {
        id: resetTimer;
        interval: 0;
        repeat: false;
        onTriggered: {
            CurveLogic.stopEditingCurve();
            CurveLogic.editCurve(root);
        }
    }

    property var resetTimer: resetTimer;
}
