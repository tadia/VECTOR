import QtQuick
import QtQuick.Shapes

Item {
    id: root;
    x: 0;
    y: 0;
    width: parent.width;
    height: parent.height;

    property real x1;
    property real y1;
    property real x2;
    property real y2;
    property real x3;
    property real y3;

    property bool symmetric: true;
    property int num;
    property int role;
    property bool editable: false;
    property bool splined: true;

    z: editable?1:0;

    Shape {
        visible: splined;

        preferredRendererType: Shape.CurveRenderer;

        ShapePath {
            strokeWidth: 2;
            strokeColor: "black";
            fillColor: "transparent";

            startX: x1; startY: y1;

            PathLine { x: x2; y: y2 }
            PathLine { x: x3; y: y3 }
        }
    }

    CurveSplineHandle { id: h1; role: 0; visible: root.role!=0 && splined }
    CurveSplineHandle { id: h2; role: 1; }
    CurveSplineHandle { id: h3; role: 2; visible: root.role!=2 && splined }

    function redraw() {
        h1.x = x1 - h1.width / 2;
        h1.y = y1 - h1.width / 2;
        h2.x = x2 - h2.width / 2;
        h2.y = y2 - h2.width / 2;
        h3.x = x3 - h3.width / 2;
        h3.y = y3 - h3.width / 2;
    }

    function moveBy(moveX, moveY) {
        x1 += moveX; y1 += moveY;
        x2 += moveX; y2 += moveY;
        x3 += moveX; y3 += moveY;
    }

    Component.onCompleted: redraw();
}
