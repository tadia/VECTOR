.import "sharedState.js" as Shared

var popupType = Qt.createComponent("OptionPopup.qml");
var optionType = Qt.createComponent("Option.qml");
var newDialogType = Qt.createComponent("NewFileDialog.qml");

function openPopup(x, y, id, window) {
    if (Shared.openedPopup && Shared.openedPopup.popupID === id) {
        closePopup();
        return;
    }

    if (Shared.paramEditor) closeParamEditor();

    closePopup();

    if (y < 8) y = 8;
    var popup = popupType.createObject(window, { "x": x, "y": y });
    var options = Shared.popups[id].options;
    popup.popupID = id;

    for (var i = 0; i < options.length; i++) {
        var optionData = options[i];
        var option = optionType.createObject(popup, {
            "text": optionData.text,
            "iconSource": optionData.iconSource,
            "type": i==0?0:(i==options.length-1?2:1),
            "selected": Shared.popups[id].type === 1 && Shared.popups[id].selected === i
        });
        option.optionID = i;
    }

    Shared.openedPopup = popup;
}

function closePopup() {
    if (Shared.openedPopup) {
        Shared.openedPopup.destroy();
        Shared.openedPopup = null;
    }
}

function editParams() {
    if (Shared.paramEditor) {
        Shared.selectedObjects.forEach(function(shape) {
            shape.shape.fillColor = Qt.rgba(
                Shared.paramEditor.fillRed / 255,
                Shared.paramEditor.fillGreen / 255,
                Shared.paramEditor.fillBlue / 255,
                Shared.paramEditor.fillAlpha
            );

            shape.shape.strokeColor = Qt.rgba(
                Shared.paramEditor.strokeRed / 255,
                Shared.paramEditor.strokeGreen / 255,
                Shared.paramEditor.strokeBlue / 255,
                Shared.paramEditor.strokeAlpha
            );

            shape.shape.realStrokeWidth = Shared.paramEditor.strokeWidth;
            shape.shape.strokeWidth = Shared.paramEditor.strokeWidth * Shared.transformScale;
        });
    }
}

function closeParamEditor() {
    Shared.paramEditor.destroy();
    Shared.paramEditor = null;
    Shared.window.sidebar.state = "open";
    Shared.boundingBox.visible = true;
}

function openParamEditor() {
    if (Shared.selectedObjects.length == 0) return;

    // Determine default values
    var fillColor = null;
    var strokeColor = null;
    var strokeWidth = null;

    Shared.selectedObjects.forEach(function(shape) {
        if (fillColor === null && fillColor !== undefined) fillColor = shape.shape.fillColor;
        else if (!Qt.colorEqual(fillColor, shape.shape.fillColor)) fillColor = Qt.rgba(0, 0, 0, 1);
        if (strokeColor === null && fillColor !== undefined) strokeColor = shape.shape.strokeColor;
        else if (!Qt.colorEqual(strokeColor, shape.shape.strokeColor)) strokeColor = Qt.rgba(0, 0, 0, 1);
        if (strokeWidth === null && fillColor !== undefined) strokeWidth = shape.shape.strokeWidth;
        else if (strokeWidth !== shape.shape.strokeWidth) strokeWidth = 0;
    });

    if (fillColor === null) fillColor = Qt.rgba(0, 0, 0, 1);
    if (strokeColor === null) strokeColor = Qt.rgba(0, 0, 0, 1);
    if (strokeWidth === null) strokeWidth = 0;

    // Open parameter editor
    Shared.paramEditor = paramEditorType.createObject(Shared.window, {
        "updateValues": editParams,
        "closeEditor": closeParamEditor,
        "fillRed": fillColor.r * 255,
        "fillGreen": fillColor.g * 255,
        "fillBlue": fillColor.b * 255,
        "fillAlpha": fillColor.a,
        "strokeRed": strokeColor.r * 255,
        "strokeGreen": strokeColor.g * 255,
        "strokeBlue": strokeColor.b * 255,
        "strokeAlpha": strokeColor.a,
        "strokeWidth": strokeWidth,
        "closeEditorStart": function() {
            Shared.window.sidebar.state = "open";
        }
    });
    Shared.boundingBox.visible = false;
}
