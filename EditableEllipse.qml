import "sharedState.js" as Shared

import QtQuick
import QtQuick.Shapes

EditableShape {
    id: root;
    type: 1;

    ShapePath {
        property real realStrokeWidth;
        strokeColor: "transparent"
        startX: root.width/2; startY: 0

        PathArc {
            x: root.width/2; y: root.height;
            radiusX: root.width/2; radiusY: root.height/2
            useLargeArc: true
        }

        PathArc {
            x: root.width/2; y: 0;
            radiusX: root.width/2; radiusY: root.height/2
            useLargeArc: true
        }

        Component.onCompleted: {
            root.shape = this;
        }
    }

    function recalcDimensions() {
        x = realX * Shared.transformScale + Shared.transformX;
        y = realY * Shared.transformScale + Shared.transformY;
        width = realWidth * Shared.transformScale;
        height = realHeight * Shared.transformScale;
    }
}
