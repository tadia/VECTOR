import QtQuick
import QtQuick.Controls

import "sharedState.js" as Shared
import "curve_logic.js" as CurveLogic

Rectangle {
    id: root;
    height: width;
    state: "default";

    Image {
        id: icon;
        anchors.centerIn: parent;
        source: "/res/plus.svg";
        sourceSize.width: 32
        sourceSize.height: 32
    }

    property var icon: icon;

    MouseArea {
        id: ma
        anchors.fill: parent;

        onPressed: root.state = "pressed";
        onReleased: {
            root.state = "default";
            CurveLogic.toggleAddingSplines();
        }
    }

    states: [
        State {
            name: "default"
            PropertyChanges { target: root; color: "#000" }
        },
        State {
            name: "pressed"
            PropertyChanges { target: root; color: "#225705" }
        }
    ]

    transitions: [
        Transition {
            from: "pressed"
            to: "default"

            ColorAnimation {
                duration: 150
            }
        }
    ]

    Component.onCompleted: Shared.addNewSplineButton = this;
}
