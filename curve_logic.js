.import "sharedState.js" as Shared

var splineType = Qt.createComponent("CurveSpline.qml");
var curveType = Qt.createComponent("Curve.qml");
var newButtonType = Qt.createComponent("NewSplineButton.qml");

function splinesToSVG(splines) {
    var output = "";
    for (var i = 0; i < splines.length; i++) {
        var spline = splines[i];
        if (i > 0) output += " ";
        switch (spline.type) {
            case 0:
                output += "L " + spline.x + " " + spline.y;
                break;
            case 1:
                output += "C " + spline.x1 + " " + spline.y1 + ", " + spline.x2 + " " + spline.y2 + ", " + spline.x + " " + spline.y;
                break;
        }
    }
    return output;
}

function splinesToRealSVG(curve) {
    var splines = curve.svgSplines;
    var output = "M " + curve.realX + " " + curve.realY;
    for (var i = 0; i < splines.length; i++) {
        var spline = splines[i];
        output += " ";
        switch (spline.type) {
            case 0:
                output += "L " + (spline.realX + curve.realX) + " " + (spline.realY + curve.realY);
                break;
            case 1:
                output += "C " + (spline.realX1 + curve.realX) + " " + (spline.realY1 + curve.realY) + ", " + (spline.realX2 + curve.realX) + " " + (spline.realY2 + curve.realY) + ", " + (spline.realX + curve.realX) + " " + (spline.realY + curve.realY);
                break;
        }
    }
    return output;
}

function createCurveSegment(x, y) {
    var role = 0;

    if (Shared.curveSegments[0]) {
        var startX = Shared.curveSegments[0].x2;
        var startY = Shared.curveSegments[0].y2;

        if (Math.abs(startX - x) <= 16 && Math.abs(startY - y) <= 16) {
            Shared.finishCurve = true;
            x = startX;
            y = startY;
            role = 2;
        } else {
            role = 1;
        }
    }

    Shared.currentCurveSegment = splineType.createObject(null, {
        "parent": Shared.canvas,
        "x1": x,
        "y1": y,
        "x2": x,
        "y2": y,
        "x3": x,
        "y3": y,
        "role": role,
        "num": Shared.curveSegments.length
    });

    Shared.curveSegments.push(Shared.currentCurveSegment);

    if (!Shared.currentCurve) {
        Shared.currentCurve = curveType.createObject(Shared.canvas, {
            "x": x,
            "y": y,
            "z": Shared.objects.length
        });
    }
}

function repositionCurveSegment(x, y) {
    Shared.currentCurveSegment.x3 = x;
    Shared.currentCurveSegment.y3 = y;
    Shared.currentCurveSegment.x1 = Shared.currentCurveSegment.x2 * 2 - x;
    Shared.currentCurveSegment.y1 = Shared.currentCurveSegment.y2 * 2 - y;
    Shared.currentCurveSegment.redraw();
}

function finalizeCurveSegment(x, y, pointy) {
    if (!Shared.currentCurve.isNew) {
        if (pointy) {
            Shared.currentCurve.svgSplines.push({
                "type": 1,
                "x": Shared.currentCurveSegment.x2 - Shared.currentCurve.x,
                "y": Shared.currentCurveSegment.y2 - Shared.currentCurve.y,
                "x1": Shared.previousCurveSegment.x2 - Shared.currentCurve.x,
                "y1": Shared.previousCurveSegment.y2 - Shared.currentCurve.y,
                "x2": Shared.currentCurveSegment.x2 - Shared.currentCurve.x,
                "y2": Shared.currentCurveSegment.y2 - Shared.currentCurve.y,
                "symmetric": true,
                "splined": false
            });
        } else {
            Shared.currentCurve.svgSplines.push({
                "type": 1,
                "x": Shared.currentCurveSegment.x2 - Shared.currentCurve.x,
                "y": Shared.currentCurveSegment.y2 - Shared.currentCurve.y,
                "x1": Shared.previousCurveSegment.x3 - Shared.currentCurve.x,
                "y1": Shared.previousCurveSegment.y3 - Shared.currentCurve.y,
                "x2": Shared.currentCurveSegment.x1 - Shared.currentCurve.x,
                "y2": Shared.currentCurveSegment.y1 - Shared.currentCurve.y,
                "symmetric": true,
                "splined": true
            });
        }

        Shared.currentCurve.svg.path = splinesToSVG(Shared.currentCurve.svgSplines);
    } else {
        Shared.currentCurve.isNew = false;
    }

    if (Shared.finishCurve) {
        Shared.objects.push(Shared.currentCurve);
        Shared.currentCurve.generateDimensions();
        Shared.currentCurve.path.strokeWidth = 0;
        Shared.currentCurve.path.strokeColor = "transparent";
        Shared.currentCurve.path.fillColor = Qt.rgba(0, 0, 0, 1);
        Shared.currentCurve.closed = true;
        Shared.currentCurve.svgSplines[0].symmetric = false;
        Shared.currentCurve.path.strokeStyle = Shared.currentCurve.path.finishedStrokeStyle;

        // Reset everything
        Shared.curveSegments.forEach(function (segment) {
            segment.destroy();
        });
        Shared.drawObject = null;
        Shared.curveSegments = [];
        Shared.previousCurveSegment = null;
        Shared.currentCurve = null;
        Shared.finishCurve = false;
    } else {
        Shared.previousCurveSegment = Shared.currentCurveSegment;
    }

    Shared.currentCurveSegment = null;
}

function moveSegment(num) {
    var svg = Shared.currentCurve.svgSplines;
    var cs = Shared.curveSegments[num];
    var posX = Shared.currentCurve.x;
    var posY = Shared.currentCurve.y;
    var len = Shared.curveSegments.length;
    var closed = Shared.currentCurve.closed;

    if (num === 0) {
        var diffX = posX - cs.x2;
        var diffY = posY - cs.y2;
        Shared.currentCurve.x = cs.x2;
        Shared.currentCurve.y = cs.y2;

        svg.forEach(function (element) {
            element.x += diffX;
            element.y += diffY;
            element.x1 += diffX;
            element.y1 += diffY;
            element.x2 += diffX;
            element.y2 += diffY;
        });

        svg[0].x1 = cs.x3 - posX;
        svg[0].y1 = cs.y3 - posY;
        if (closed) {
            svg[svg.length - 1].x = 0;
            svg[svg.length - 1].y = 0;
            svg[svg.length - 1].x2 = cs.x1 - posX;
            svg[svg.length - 1].y2 = cs.y1 - posY;
        }
    } else if (num === len - 1 && !closed) {
        svg[num - 1].x = cs.x2 - posX;
        svg[num - 1].y = cs.y2 - posY;
        svg[num - 1].x2 = cs.x1 - posX;
        svg[num - 1].y2 = cs.y1 - posY;
    } else {
        svg[num - 1].x = cs.x2 - posX;
        svg[num - 1].y = cs.y2 - posY;
        svg[num - 1].x2 = cs.x1 - posX;
        svg[num - 1].y2 = cs.y1 - posY;
        svg[num].x1 = cs.x3 - posX;
        svg[num].y1 = cs.y3 - posY;
    }

    Shared.currentCurve.svg.path = splinesToSVG(svg);
}

function editCurve(curve) {
    Shared.currentCurve = curve;
    Shared.curveSidebar.state = "open";
    var svg = curve.svgSplines;

    for (var i = 0; i < svg.length; i++) {
        if (i == 0) {
            Shared.curveSegments.push(splineType.createObject(Shared.canvas, {
                "x1": Shared.currentCurve.closed?(curve.x + svg[svg.length - 1].x2):(curve.x - svg[i].x1),
                "y1": Shared.currentCurve.closed?(curve.y + svg[svg.length - 1].y2):(curve.y - svg[i].y1),
                "x2": curve.x,
                "y2": curve.y,
                "x3": curve.x + svg[i].x1,
                "y3": curve.y + svg[i].y1,
                "editable": true,
                "role": Shared.currentCurve.closed?1:0,
                "num": 0,
                "symmetric": svg[i].symmetric,
                "splined": svg[i].splined
            }));
        } else {
            Shared.curveSegments.push(splineType.createObject(Shared.canvas, {
                "x1": curve.x + svg[i-1].x2,
                "y1": curve.y + svg[i-1].y2,
                "x2": curve.x + svg[i-1].x,
                "y2": curve.y + svg[i-1].y,
                "x3": curve.x + svg[i].x1,
                "y3": curve.y + svg[i].y1,
                "editable": true,
                "role": 1,
                "num": i,
                "symmetric": svg[i].symmetric,
                "splined": svg[i].splined
            }));
        }
    }
}

function stopEditingCurve() {
    Shared.curveSidebar.state = "closed";
    Shared.curveSegments.forEach(function(segment) {
        segment.destroy();
    });
    Shared.currentCurve.generateDimensions();
    Shared.curveSegments = [];
    Shared.currentCurve = null;
};

function deleteSelectedSegments() {
    var firstDelete = false;

    var removed = 0;

    while (Shared.selectedCurveHandles.length > 0) {
        let segment = Shared.selectedCurveHandles[0].parent;
        let element = Shared.currentCurve.svgSplines[segment.num - removed - 1];
        let elementNext = Shared.currentCurve.svgSplines[segment.num - removed];

        if (segment.num !== 0) {
            // Repair
            elementNext.x1 = element.x1;
            elementNext.y1 = element.y1;

            // Destroy spline and handle
            Shared.currentCurve.svgSplines.splice(segment.num - removed - 1, 1);
            Shared.curveSegments.splice(segment.num - removed, 1);
            Shared.selectedCurveHandles.splice(0, 1);
            segment.destroy();
            removed++;
        } else {
            firstDelete = true;
            Shared.selectedCurveHandles.splice(0, 1);
        }
    }

    // Reorder
    for (var i = 0; i < Shared.curveSegments.length; i++) {
        let segment = Shared.curveSegments[i];
        segment.num = i;
    }

    if (firstDelete) {
        // Move starting point
        var leNum = Shared.currentCurve.svgSplines.length - 1;
        var lastEl = Shared.currentCurve.svgSplines[leNum];
        var moveX = lastEl.x;
        var moveY = lastEl.y;

        Shared.currentCurve.x += moveX;
        Shared.currentCurve.y += moveY;

        // Destroy last element and first handle
        Shared.curveSegments[0].destroy();
        Shared.curveSegments.splice(0, 1);
        lastEl.destroy();
        Shared.currentCurve.svgSplines.splice(leNum, 1);

        // Reorder
        for (let i = 0; i < Shared.curveSegments.length; i++) {
            let segment = Shared.curveSegments[i];
            segment.num = i;
        }
    }

    if (Shared.curveSegments.length == 0) {
        var index = Shared.objects.indexOf(Shared.currentCurve);
        Shared.currentCurve.destroy();
        if (index > -1) Shared.objects.splice(index, 1);
        stopEditingCurve();
    } else {
        Shared.currentCurve.svg.path = splinesToSVG(Shared.currentCurve.svgSplines);
        Shared.currentCurve.resetTimer.start();
    }
}

function resymmetrifySelectedSegments(symmetric) {
    for (var i = 0; i < Shared.selectedCurveHandles.length; i++) {
        let segment = Shared.selectedCurveHandles[i].parent;
        Shared.currentCurve.svgSplines[segment.num].symmetric = symmetric;
        Shared.currentCurve.svgSplines[segment.num].splined = true;
        segment.symmetric = symmetric;
        segment.splined = true;
    }
}

function decurvifySelectedSegments(symmetric) {
    for (var i = 0; i < Shared.selectedCurveHandles.length; i++) {
        let segment = Shared.selectedCurveHandles[i].parent;
        segment.x1 = segment.x2;
        segment.y1 = segment.y2;
        segment.x3 = segment.x2;
        segment.y3 = segment.y2;
        segment.splined = false;
        moveSegment(segment.num);
        Shared.currentCurve.svgSplines[segment.num].splined = false;
    }
}

function toggleAddingSplines() {
    if (Shared.addNewSpline) {
        Shared.addNewSpline = false;
        Shared.curveSegments.forEach(function(segment) {
            segment.editable = true;
            segment.redraw();
        });
        Shared.newSplineButtons.forEach(function(button) {
            if (button.visible) button.destroy();
        });
        Shared.newSplineButtons = [];
    } else {
        Shared.addNewSpline = true;
        Shared.selectedCurveHandles.forEach(function(handle) {
            handle.deselect();
        });
        Shared.selectedCurveHandles = [];
        for (var i = 0; i < Shared.curveSegments.length; i++) {
            var segment = Shared.curveSegments[i];
            var path = Shared.currentCurve.path;

            segment.editable = false;
            segment.redraw();

            var point = path.pointAtPercent((i + 0.5) / Shared.currentCurve.svgSplines.length);
            Shared.newSplineButtons.push(newButtonType.createObject(Shared.window, { "x": Shared.currentCurve.x + point.x - 16, "y": Shared.currentCurve.y + point.y - 16, "num": i }));
        }
    }

    Shared.addNewSplineButton.icon.source = Shared.addNewSpline?"/res/x.svg":"/res/plus.svg";
}

function newSpline(num) {
    if (!Shared.currentCurve) return;

    var svg = Shared.currentCurve.svgSplines;

    // Move previous path element
    var oldX1 = svg[num].x1;
    svg[num].x1 = svg[num].x2;
    var oldY1 = svg[num].y1;
    svg[num].y1 = svg[num].y2

    // Create new path element;
    Shared.currentCurve.svgSplines.splice(num, 0, {
        "type": 1,
        "x": Shared.newSplineButtons[num].x + 16 - Shared.currentCurve.x,
        "y": Shared.newSplineButtons[num].y + 16 - Shared.currentCurve.y,
        "x1": oldX1,
        "y1": oldY1,
        "x2": oldX1,
        "y2": oldY1,
        "symmetric": false
    });

    Shared.currentCurve.svg.path = splinesToSVG(svg);

    Shared.addNewSpline = false;
    Shared.newSplineButtons.forEach(function(button) {
        if (button.visible) button.destroy();
    });
    Shared.newSplineButtons = [];
    Shared.addNewSplineButton.icon.source = "/res/plus.svg";
    Shared.currentCurve.resetTimer.start();
}
