#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>

#include "fileio.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/VECTOR/Main.qml"_qs);
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    qmlRegisterType<FileIO, 1>("FileIO", 1, 0, "FileIO");
    app.setWindowIcon(QIcon(":/res/icon.svg"));
    engine.load(url);

    return app.exec();
}
