import "popup_logic.js" as PopLogic
import "sharedState.js" as Shared
import "logic.js" as Logic
import "file_logic.js" as FileLogic

import QtQuick
import QtQuick.Layouts

Rectangle {
    id: root;

    implicitWidth: childrenRect.width + 32;
    Layout.fillWidth: true;
    height: 44;
    width: textField.width + 64;
    color: "black";

    property string text;
    property string iconSource;
    property int type;
    property int optionID;
    property bool selected: false;

    state: selected?"pressed":
                     "default";

    Image {
        anchors.verticalCenter: parent.verticalCenter;
        anchors.left: parent.left;
        anchors.leftMargin: 8;
        source: iconSource;
        sourceSize.width: 32
        sourceSize.height: 32
        z: 6
    }

    Text {
        id: textField;
        anchors.verticalCenter: parent.verticalCenter;
        anchors.left: parent.left;
        anchors.leftMargin: 48;
        text: parent.text;
        font.family: "sans-serif"
        font.pointSize: 16
        color: "#fff"
        z: 6;
    }

    MouseArea {
        id: ma
        anchors.fill: parent;

        onPressed: root.state = "pressed";
        onReleased: {
            var popup = Shared.popups[root.parent.popupID];
            if (popup.type === 1) popup.selected = optionID;
            switch (root.parent.popupID) {
                case 0:
                    Logic.deselect();
                    Shared.canvas.switchMode();
                    break;
                case 1:
                    Logic.addShape(optionID);
                    break;
                case 2:
                    switch (optionID) {
                        case 0:
                            Logic.moveSelectionToTop();
                            break;
                        case 1:
                            Logic.moveSelectionUp();
                            break;
                        case 2:
                            Logic.moveSelectionDown();
                            break;
                        case 3:
                            Logic.moveSelectionToBottom();
                            break;
                    }
                    break;
                case 3:
                    switch (optionID) {
                        case 0:
                            FileLogic.newFile();
                            break;
                        case 1:
                            FileLogic.openFile();
                            break;
                        case 2:
                            FileLogic.saveFile();
                            break;
                        case 3:
                            FileLogic.exportFile();
                            break;
                    }
            }
            PopLogic.closePopup();
        }
    }

    states: [
        State {
            name: "default"
            PropertyChanges { target: root; color: "#000" }
        },
        State {
            name: "pressed"
            PropertyChanges { target: root; color: "#225705" }
        }
    ]

    Component.onCompleted: {
        if (type != 1) {
            radius = 8;
            Qt.createComponent("OptionMask.qml").createObject(this, (type==0)?{ "anchors.bottom": this.bottom }:{ "anchors.top": this.top });
        }
    }
}
