.import "sharedState.js" as Shared
.import "boundingbox_logic.js" as BBLogic
.import "curve_logic.js" as CurveLogic

var ellipseType = Qt.createComponent("EditableEllipse.qml");
var rectangleType = Qt.createComponent("EditableRectangle.qml");

var drawBoxType = Qt.createComponent("DrawBox.qml");

function displayToRealX(x) { return (x - Shared.transformX) / Shared.transformScale; }
function displayToRealY(y) { return (y - Shared.transformY) / Shared.transformScale; }
function realToDisplayX(x) { return x * Shared.transformScale + Shared.transformX; }
function realToDisplayY(y) { return y * Shared.transformScale + Shared.transformY; }

function addShape(type) {
    Shared.drawObject = type;
}

function addShapeFinal(canvas, type, x, y, width, height) {
    var selectedType;
    switch (type) {
        case 0:
            selectedType = rectangleType;
            break;
        case 1:
            selectedType = ellipseType;
            break;
    }

    // Fix negative sizes
    if (width < 0) {
        width = -width;
        x -= width;
    }

    if (height < 0) {
        height = -height;
        y -= height;
    }

    var shape = selectedType.createObject(canvas, {
        "x": x,
        "realX": displayToRealX(x),
        "y": y,
        "realY": displayToRealY(y),
        "width": width,
        "realWidth": width / Shared.transformScale,
        "height": height,
        "realHeight": height / Shared.transformScale,
        "z": Shared.objects.length + 1
    });
    shape.setColor(Qt.rgba(0, 0, 0, 1));
    Shared.objects.push(shape);
}

function createDrawBox(x, y) {
    Shared.drawBox = drawBoxType.createObject(null, { "parent": canvas, "x": x, "y": y, "width": 0, "height": 0 });
}

// Select only this object
function select(shape, canvas) {
    Shared.selectedObjects.forEach(function(shape) {
        shape.isSelected = false;
    });
    Shared.selectedObjects = [ shape ];
    shape.isSelected = true;
    BBLogic.drawBoundingBox(canvas);
}

// Select another object
function multiSelect(shape, canvas) {
    if (!shape.isSelected) {
        Shared.selectedObjects.push(shape);
        shape.isSelected = true;
        BBLogic.drawBoundingBox(canvas);
    }
}

// Select only this object if outside the current selection
function moveSelect(shape, canvas) {
    if (!shape.isSelected) {
        Shared.selectedObjects.forEach(function(shape) {
            shape.isSelected = false;
        });
        Shared.selectedObjects = [ shape ];
        shape.isSelected = true;
        BBLogic.drawBoundingBox(canvas);
    }
}

function deselect() {
    Shared.selectedObjects.forEach(function(shape) {
        shape.isSelected = false;
    });

    Shared.selectedObjects = [];
    if (Shared.boundingBox) {
        Shared.boundingBox.destroy();
    }
    Shared.boundingBox = null;
}

function startRepositionSelection() {
    Shared.selectedObjects.forEach(function(shape) {
        shape.oldX = shape.x;
        shape.oldY = shape.y;
        if (shape.type === 2) {
            shape.svgSplines.forEach(function(element) {
                element.oldX = element.x;
                element.oldY = element.y;
                element.oldX1 = element.x1;
                element.oldY1 = element.y1;
                element.oldX2 = element.x2;
                element.oldY2 = element.y2;
            });
        } else {
            shape.oldWidth = shape.width;
            shape.oldHeight = shape.height;
        }
    });

    Shared.boundingBox.oldX = Shared.boundingBox.x;
    Shared.boundingBox.oldY = Shared.boundingBox.y;
    Shared.boundingBox.oldWidth = Shared.boundingBox.width;
    Shared.boundingBox.oldHeight = Shared.boundingBox.height;
}

function repositionSelection() {
    Shared.selectedObjects.forEach(function(shape) {
        if (shape.type === 2) {
            shape.x = Shared.boundingBox.x + (shape.oldX - Shared.boundingBox.oldX) / Shared.boundingBox.oldWidth * Shared.boundingBox.width;
            shape.y = Shared.boundingBox.y + (shape.oldY - Shared.boundingBox.oldY) / Shared.boundingBox.oldHeight * Shared.boundingBox.height;
            shape.svgSplines.forEach(function(element) {
                element.x = element.oldX / Shared.boundingBox.oldWidth * Shared.boundingBox.width;
                element.y = element.oldY / Shared.boundingBox.oldHeight * Shared.boundingBox.height;
                element.x1 = element.oldX1 / Shared.boundingBox.oldWidth * Shared.boundingBox.width;
                element.y1 = element.oldY1 / Shared.boundingBox.oldHeight * Shared.boundingBox.height;
                element.x2 = element.oldX2 / Shared.boundingBox.oldWidth * Shared.boundingBox.width;
                element.y2 = element.oldY2 / Shared.boundingBox.oldHeight * Shared.boundingBox.height;
            });
            shape.svg.path = CurveLogic.splinesToSVG(shape.svgSplines);
            shape.generateDimensions();
        } else {
            shape.x = Shared.boundingBox.x + (shape.oldX - Shared.boundingBox.oldX) / Shared.boundingBox.oldWidth * Shared.boundingBox.width;
            shape.y = Shared.boundingBox.y + (shape.oldY - Shared.boundingBox.oldY) / Shared.boundingBox.oldHeight * Shared.boundingBox.height;
            shape.width = shape.oldWidth / Shared.boundingBox.oldWidth * Shared.boundingBox.width;
            shape.height = shape.oldHeight / Shared.boundingBox.oldHeight * Shared.boundingBox.height;

            // Fix negative sizes
            if (shape.width < 0) {
                shape.width = -shape.width;
                shape.x -= shape.width;
            }

            if (shape.height < 0) {
                shape.height = -shape.height;
                shape.y -= shape.height;
            }

            shape.realX = displayToRealX(shape.x);
            shape.realY = displayToRealY(shape.y);
            shape.realWidth = shape.width / Shared.transformScale;
            shape.realHeight = shape.height / Shared.transformScale;
        }
    });
}

function moveSelectionBy(originator, x, y) {
    if (Shared.selectedObjects.length > 1 && originator.isSelected) {
        Shared.selectedObjects.forEach(function(shape) {
            shape.x += x;
            shape.y += y;
            shape.realX = displayToRealX(shape.x);
            shape.realY = displayToRealY(shape.y);
        });
    } else {
        originator.x += x;
        originator.y += y;
        originator.realX = displayToRealX(originator.x);
        originator.realY = displayToRealY(originator.y);
    }
}

function deleteSelection() {
    Shared.selectedObjects.forEach(function(shape) {
        shape.destroy();

        var index = Shared.objects.indexOf(shape);
        if (index > -1) Shared.objects.splice(index, 1);
    });

    Shared.selectedObjects = [];
    if (Shared.boundingBox) {
        Shared.boundingBox.destroy();
    }
    Shared.boundingBox = null;
}

function sortSelectionFunction(a, b) {
    if (a.z === b.z) {
        return 0;
    }
    else {
        return (a.z < b.z) ? -1 : 1;
    }
}

function sortSelection() {
    Shared.selectedObjects.sort(sortSelectionFunction);
}

function reorderObjects() {
    for (var i = 0; i < Shared.objects.length; i++) {
        Shared.objects[i].z = i + 1;
    }
}

function moveSelectionToTop() {
    sortSelection();

    for (var i = 0; i < Shared.selectedObjects.length; i++) {
        let index = Shared.objects.indexOf(Shared.selectedObjects[i]);
        Shared.objects.push(Shared.objects.splice(index, 1)[0]);
    }

    reorderObjects();
}

function moveSelectionUp() {
    sortSelection();

    var insertPos = 0;
    var movedElements = [];

    var i = 0;

    for (; i < Shared.selectedObjects.length; i++) {
        let index = Shared.objects.indexOf(Shared.selectedObjects[i]);
        movedElements.push(Shared.objects.splice(index, 1)[0]);
        insertPos = index + 1;
    }

    Shared.objects.splice(insertPos, 0, ...movedElements);

    reorderObjects();
}

function moveSelectionDown() {
    sortSelection();

    var insertPos = Shared.objects.length;
    var movedElements = [];

    var i = 0;

    for (; i < Shared.selectedObjects.length; i++) {
        let index = Shared.objects.indexOf(Shared.selectedObjects[i]);
        movedElements.push(Shared.objects.splice(index, 1)[0]);
        insertPos = Math.min(index - 1, insertPos);
    }

    Shared.objects.splice(insertPos, 0, ...movedElements);

    reorderObjects();
}

function moveSelectionToBottom() {
    sortSelection();

    for (var i = Shared.selectedObjects.length - 1; i >= 0; i--) {
        let index = Shared.objects.indexOf(Shared.selectedObjects[i]);
        Shared.objects.unshift(Shared.objects.splice(index, 1)[0]);
    }

    reorderObjects();
}

function resetContext(width, height) {
    Shared.mode = 0;
    Shared.switcher.iconSource = "/res/move.svg";
    Shared.canvas.switchMode();

    deselect();
    if (Shared.currentCurve) CurveLogic.stopEditingCurve();

    Shared.objects.forEach(function(shape) {
        shape.destroy();
    });
    Shared.objects = [];

    Shared.boundary.x = 0;
    Shared.boundary.y = 0;
    Shared.boundary.width = width;
    Shared.boundary.realWidth = width;
    Shared.boundary.height = height;
    Shared.boundary.realHeight = height;

    Shared.transformScale = 1;
    Shared.transformX = 0;
    Shared.transformY = 0;
}
