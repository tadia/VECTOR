import QtQuick
import QtQuick.Layouts

ColumnLayout {
    id: root;

    width: childrenRect.width;
    height: childrenRect.height;
    z: 5;
    property int popupID;
    spacing: 0;
}
