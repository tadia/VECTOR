import QtQuick

import "curve_logic.js" as CurveLogic
import "sharedState.js" as Shared

Rectangle {
    id: root;
    width: parent.editable?32:(((parent.num==0)&&role==1&&!Shared.addNewSpline)?32:10);
    height: parent.editable?32:(((parent.num==0)&&role==1&&!Shared.addNewSpline)?32:10);
    border.width: 2;
    border.color: "black";
    radius: parent.splined?2:32;
    z: parent.z;
    color: parent.editable?"white":"transparent";
    rotation: (!parent.symmetric&&role==1)?45:0;

    property int role;
    property bool selected;

    MouseArea {
        id: ma;
        x: 0;
        y: 0;
        width: root.width;
        height: root.height;
        z: parent.z;

        property int startX;
        property int startY;
        property bool accidentProtection: true;

        Timer {
            id: timer
            running: false
            repeat: false
            interval: 300

            onTriggered: {
                if (selected) {
                    selected = false;
                    root.color = "#000";

                    var index = Shared.selectedCurveHandles.indexOf(root);
                    if (index > -1) Shared.selectedCurveHandles.splice(index, 1);
                } else {
                    selected = true;
                    root.color = "#00f";
                    Shared.selectedCurveHandles.push(root);
                }
            }
        }

        onPressed: {
            startX = mouseX
            startY = mouseY
            accidentProtection = true;
            if (!selected) parent.color = "black";

            if (role==1) timer.start();
        }

        onPositionChanged: {
            if (accidentProtection && (Math.sqrt(Math.pow(mouseX - startX, 2) + Math.pow(mouseY - startY, 2)) < 4)) return;
            accidentProtection = false;
            timer.stop();

            var spline = parent.parent;

            var halfSize = this.width / 2;

            parent.x += mouseX - startX;
            parent.y += mouseY - startY;

            switch (role) {
                case 0:
                    spline.x1 = parent.x + halfSize;
                    spline.y1 = parent.y + halfSize;

                    if (spline.symmetric) {
                        spline.x3 = spline.x2 * 2 - spline.x1;
                        spline.y3 = spline.y2 * 2 - spline.y1;
                    }
                    break;
                case 1:
                    if (selected) Shared.selectedCurveHandles.forEach(function(handle) {
                        handle.parent.moveBy(mouseX - startX, mouseY - startY);
                        handle.parent.redraw();
                        CurveLogic.moveSegment(handle.parent.num);
                    })
                    else spline.moveBy(mouseX - startX, mouseY - startY);
                    break;
                case 2:
                    spline.x3 = parent.x + halfSize;
                    spline.y3 = parent.y + halfSize;

                    if (spline.symmetric) {
                        spline.x1 = spline.x2 * 2 - spline.x3;
                        spline.y1 = spline.y2 * 2 - spline.y3;
                    }
                    break;
            }

            CurveLogic.moveSegment(spline.num);

            spline.redraw();
        }

        onReleased: {
            timer.stop();
            if (!selected) parent.color = "white";
        }
    }

    function deselect() {
        selected = false;
        root.color = "#fff";
    }
}
