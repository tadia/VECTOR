import QtQuick
import QtQuick.Controls

Rectangle {
    id: root;
    property string iconSource;
    property var onClick;
    height: width;
    state: "default";

    Image {
        anchors.centerIn: parent;
        source: iconSource;
        sourceSize.width: 32
        sourceSize.height: 32
    }

    MouseArea {
        id: ma
        anchors.fill: parent;

        onPressed: root.state = "pressed";
        onReleased: {
            root.state = "default";
            onClick(parent);
        }
    }

    states: [
        State {
            name: "default"
            PropertyChanges { target: root; color: "#000" }
        },
        State {
            name: "pressed"
            PropertyChanges { target: root; color: "#225705" }
        }
    ]

    transitions: [
        Transition {
            from: "pressed"
            to: "default"

            ColorAnimation {
                duration: 150
            }
        }
    ]
}
